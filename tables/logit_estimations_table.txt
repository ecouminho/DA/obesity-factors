		(1)	(2)
		m_logit	f_logit
VARIABLES	LABELS	Obesity	Obesity
			
age	Age in years	0.012***	0.012***
		(0.002)	(0.002)
smoking	Currently Smoking	-0.189**	-0.038
		(0.082)	(0.081)
physical	Hours Weekdays Physical Activities	-0.281***	-0.237***
		(0.052)	(0.049)
income	Net Income Last Month	-0.000	-0.000***
		(0.000)	(0.000)
2.hdiet	Follows Health-Conscious Diet = 2, [2] Strong	-0.006	0.080
		(0.167)	(0.124)
3.hdiet	Follows Health-Conscious Diet = 3, [3] A little	0.664***	0.724***
		(0.161)	(0.124)
4.hdiet	Follows Health-Conscious Diet = 4, [4] Not at All	0.581***	0.521***
		(0.194)	(0.195)
Constant	Constant	-2.167***	-2.186***
		(0.213)	(0.167)
			
Observations		5,186	6,069
Standard errors in parentheses			
*** p<0.01, ** p<0.05, * p<0.1			
Source: Own calculations.			
