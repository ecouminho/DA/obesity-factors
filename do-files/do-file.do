*! version 3.0                    <15Mar2017>                    Nelson Estevão
* ==============================================================================
* Program Name: do-file1.do
* Student Name: Nelson Miguel de Oliveira Estevão      Student Number: 10004154
* ------------------------------------------------------------------------------
* Gottfried Wilhelm Leibniz Universität Hannover
* School of Economics and Management
* Institute of Labour Economics
* ------------------------------------------------------------------------------
* Data Analytics                                        Winter Semester 2016/17
* Topic Number 9
* ==============================================================================

clear all
set more off // does not pause when review window is full
set rmsg on  // return how long each command took to execute and what time it completed execution


capture log close // closes any unclosed logs
log using "$l_dir\do-file1.txt", text replace name(do_file1) // use log file with text format

use "$d_dir\bep.dta", clear // clear memory before opening data
    lab lang EN // change variables label to english

*===============================================================================
* # 1. PREPARE THE DATA
*===============================================================================

  // rename variables

    replace bep96=. if bep96==-1       // replacing -1 for missing values
    rename bep96 height

    replace bep97=. if bep97==-1       // replacing -1 for missing values
    rename bep97 weight

    replace bep12601=0 if bep12601==2  // transforming it in a dummy variable
    rename bep12601 sex

    replace bep0101=. if bep0101==-1   // replacing -1 for missing values
    rename bep0101 health
    tab health

    replace bep0102=. if bep0102==-1   // replacing -1 for missing values
    rename bep0102 sleep
    tab sleep

    replace bep5702=0 if bep5702==-2   // replacing -2 for no income last month
    replace bep5702=. if bep5702==-1   // replacing -1 for missing values
    replace bep5702=. if bep5702==-3   // replacing -3 for missing values
    rename bep5702 income

    replace bep0508=0 if bep0508==-2   // replacing -2 for 0 hours of physical activities
    replace bep0508=. if bep0508==-3   // replacing -3 for missing values
    rename bep0508 physical

    replace bep9401=. if bep9401==-1   // replacing -1 for missing values
    replace bep9401=0 if bep9401==2    // transforming it in a dummy variable
    rename bep9401 smoking

    replace bep127=. if bep127==-1     // replacing -1 for missing values
    rename bep127 marital
    tab marital

    replace bep12603=. if bep12603==-5 // replacing -5 for missing values
    rename bep12603 ybirth

    replace bep95=. if bep95==-1       // replacing -1 for missing values
    replace bep95=. if bep95==-5       // replacing -5 for missing values
    rename bep95 hdiet

  // create new variables

    gen bmi = weight /(height*height*0.01*0.01) // calculating the BMI

    gen age = 2014-ybirth       // generating the age according to the year 2014
    gen age2 = age^2            // generating the square of age

    gen lnincome = ln(income) 	// generating the natural log of income

    gen obese = 1 if bmi>=30    /* generating a dummy variable for using  */
    replace obese = 0 if bmi<30	/* it in the logit model                  */

  // apply label to new variables

    label var bmi "Body Mass Index (Kg/m2)" // apply variable label
    label var age "Age in years"            // apply variable label
    label var age2 "Age squared"            // apply variable label
    label var obese "Obesity"               // apply variable label
    label var lnincome "Ln of Income"       // apply variable label

  label define lblsex 0"[0] Female" 1"[1] Male" // create a value label
    label values sex lblsex         // apply the value label

  label define lblsmoking 0"[0] No" 1"[1] Yes"  // create a value label
    label values smoking lblsmoking // apply the value label

  label define lblobese 0"[0] No" 1"[1] Yes"    // create a value label
    label values obese lblobese     // apply the value label

  label define lblhdiet 1"[1] Very Strong" 2"[2] Strong" 3"[3] A little" 4"[4] Not at All" // create a value label
    label values hdiet lblhdiet

*===============================================================================
* # 2. DESCRIPTIVE STATISTICS
*===============================================================================

    drop if mi(age, height, weight, sex, hdiet, marital, smoking, physical, income, sleep, health) // droping observations with missing values
    keep bmi obese sex age age2 hdiet marital smoking physical income lnincome sleep health   // keep only the variables to be used
    order bmi age income obese sex smoking hdiet marital physical sleep health  // order them

  // export the descriptive statistics table

    outreg2 using "$t_dir\summary_statistics.doc", sum(detail) word eqkeep(N mean p1 p50 p99 sd min max) replace
    outreg2 using "$t_dir\summary_statistics.tex", sum(detail) tex eqkeep(N mean p1 p50 p99 sd min max) replace

  // just for male
    preserve
        drop if sex==0
        keep bmi obese age hdiet marital smoking physical income sleep health
        outreg2 using "$t_dir\m_summary_statistics.doc", sum(detail) word eqkeep(N mean p1 p50 p99 sd min max) replace
        outreg2 using "$t_dir\m_summary_statistics.tex", sum(detail) tex eqkeep(N mean p1 p50 p99 sd min max) replace
    restore

  // just for female
    preserve
        drop if sex==1
        keep bmi obese age hdiet marital smoking physical income sleep health
        outreg2 using "$t_dir\f_summary_statistics.doc", sum(detail) word eqkeep(N mean p1 p50 p99 sd min max) replace
        outreg2 using "$t_dir\f_summary_statistics.tex", sum(detail) tex eqkeep(N mean p1 p50 p99 sd min max) replace
    restore

  // produce graphs and export

    histogram bmi, percent normal xlabel(#25) graphregion(fcolor(white) lcolor(white)) xsize(13) ysize(10)
        graph export "$p_dir\histogram_bmi.png", as(png) width(3300) replace

    histogram bmi, percent normal xlabel(#20) by(sex) by(, graphregion(fcolor(white) lcolor(white))) xsize(13) ysize(10)
        graph export "$p_dir\histogram_bmi-sex.png", as (png) width(3300) replace

    scatter bmi income, graphregion(fcolor(white) lcolor(white)) xsize(13) ysize(10)
        graph export "$p_dir\scatter_bmi-income.png", as (png) width(3300) replace

    graph bar (percent), over(sex) over(smoking) graphregion(fcolor(white) lcolor(white)) xsize(13) ysize(10)
        graph export "$p_dir\graph-bar_sex-smoking.png", as (png) width(3300) replace

    histogram physical, discrete percent normal xlab(#13) ylab(#20) graphregion(fcolor(white) lcolor(white)) xsize(13) ysize(10)
        graph export "$p_dir\histogram_physical.png", as (png) width(3300) replace

    histogram marital, discrete percent xlab(#10) ylab(#20) graphregion(fcolor(white) lcolor(white)) xsize(13) ysize(10)
        graph export "$p_dir\histogram_marital.png", as (png) width(3300) replace

    histogram health, discrete normal graphregion(fcolor(white) lcolor(white)) xsize(13) ysize(10)
        graph export "$p_dir\histogram_health.png", as (png) width(3300) replace

    histogram sleep, discrete normal graphregion(fcolor(white) lcolor(white)) xsize(13) ysize(10)
        graph export "$p_dir\histogram_sleep.png", as (png) width(3300) replace

*===============================================================================
* # 3. REGRESSIONS
*===============================================================================

regress bmi age age2 i.hdiet i.marital smoking physical i.health income
regress bmi age age2 i.hdiet i.marital smoking physical i.health income if sex==0
regress bmi age age2 i.hdiet i.marital smoking physical i.health income if sex==1
di ((272156.213-(159590.451+107528.079))/(159590.451+107528.079))*((11255-(2*27))/(27)) // Chow Test

  // OLS
    regress bmi age age2 smoking if sex==1 // if it is a male
        est store m_ols1
    regress bmi age age2 smoking if sex==0 // if it is a female
        est store f_ols1
    regress bmi age age2 smoking physical income i.hdiet if sex==1 // if it is a male
        est store m_ols2
    regress bmi age age2 smoking physical income i.hdiet  if sex==0 // if it is a female
        est store f_ols2
    regress bmi age age2 smoking physical income i.hdiet i.marital if sex==1 // if it is a male
        est store m_ols3
    regress bmi age age2 smoking physical income i.hdiet i.marital if sex==0 // if it is a female
        est store f_ols3

outreg2 [m_ols1 m_ols2 m_ols3 f_ols1 f_ols2 f_ols3] using "$t_dir\ols_estimations_table.doc", word replace label addnote(Source: Own calculations.) dec(3) alpha(0.01, 0.05, 0.1)
outreg2 [m_ols1 m_ols2 m_ols3 f_ols1 f_ols2 f_ols3] using "$t_dir\ols_estimations_table.tex", tex replace label addnote(Source: Own calculations.) dec(3) alpha(0.01, 0.05, 0.1)

  // Logit
    logit obese age smoking physical income i.hdiet if sex==1 // if it is a male
        est store m_logit
    logit obese age smoking physical income i.hdiet if sex==0 // if it is a female
        est store f_logit

    logistic obese age smoking physical income i.hdiet if sex==1 // if it is a male
    logistic obese age smoking physical income i.hdiet if sex==0 // if it is a female

outreg2 [m_logit f_logit] using "$t_dir\logit_estimations_table.doc", word replace label addnote(Source: Own calculations.) dec(3) alpha(0.01, 0.05, 0.1)
outreg2 [m_logit f_logit] using "$t_dir\logit_estimations_table.tex", tex replace label addnote(Source: Own calculations.) dec(3) alpha(0.01, 0.05, 0.1)
outreg2 [m_logit f_logit] using "$t_dir\logit_eform_estimations_table.tex", tex eform replace label addnote(Source: Own calculations.) dec(3) alpha(0.01, 0.05, 0.1)


log close do_file1 // close the log file

exit, clear // exit without saving changes

><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><

Notes:
1. [-1] No Answer
2. [-2] Does Not Apply
3. [-3] Answer Not Valid
4. [-5] Not Included In Questionnaire Version
5. to run this do-file the package outreg2 has to be install [command: `ssc install outreg2`]
