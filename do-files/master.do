*! version 1.0                    <15Feb2017>                    Nelson Estevão
* ==============================================================================
* Program Name: master-do-file.do
* Student Name: Nelson Miguel de Oliveira Estevão      Student Number: 10004154
* ------------------------------------------------------------------------------
* Gottfried Wilhelm Leibniz Universität Hannover
* School of Economics and Management
* Institute of Labour Economics
* ------------------------------------------------------------------------------
* Data Analytics                                        Winter Semester 2016/17
* Topic Number 9
* ==============================================================================

set more off
capture log close
version 14.1 	// specify version used

global c_dir "C:\Users\Nelson\OneDrive\Documents\Universidade do Minho\Licenciatura em Economia\3. Ano\1. Semestre\3. Data Analytics\6. Term Paper\do-files" // File path for folders with do-files
global d_dir "C:\Users\Nelson\OneDrive\Documents\Universidade do Minho\Licenciatura em Economia\3. Ano\1. Semestre\3. Data Analytics\6. Term Paper\dta"      // File path for folders with data
global l_dir "C:\Users\Nelson\OneDrive\Documents\Universidade do Minho\Licenciatura em Economia\3. Ano\1. Semestre\3. Data Analytics\6. Term Paper\logs"     // File path for folders with log-files
global p_dir "C:\Users\Nelson\OneDrive\Documents\Universidade do Minho\Licenciatura em Economia\3. Ano\1. Semestre\3. Data Analytics\6. Term Paper\plots"    // File path for folders with plots
global t_dir "C:\Users\Nelson\OneDrive\Documents\Universidade do Minho\Licenciatura em Economia\3. Ano\1. Semestre\3. Data Analytics\6. Term Paper\tables"   // File path for folders with tables


exit, clear

><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><

Notes:
1.
2.
3.
