ARTICLE = obesity-factors
OUTPUT  = output
PAGES   = public
TEX     = pdflatex
BIBTEX  = bibtex

all:
	Rscript -e "rmarkdown::render('$(ARTICLE).rmd', output_format = 'all', output_dir = '$(OUTPUT)')"

pdf:
	Rscript -e "rmarkdown::render('$(ARTICLE).rmd', 'pdf_document', output_dir = '$(OUTPUT)')"

html:
	Rscript -e "rmarkdown::render('$(ARTICLE).rmd', 'html_document', output_file = 'index.html', output_dir = '$(PAGES)')"

clean:
	@echo "Cleaning..."
	@echo ""
	@-curl https://raw.githubusercontent.com/nelsonmestevao/spells/master/art/maid.ascii
	@rm -rf $(OUTPUT) $(PAGES)
	@echo ""
	@echo "...✓ done!"

