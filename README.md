[gitlabci]: https://gitlab.com/ecouminho/DA/obesity-factors/commits/master
[coverage]: https://gitlab.com/ecouminho/DA/obesity-factors/commits/master


# Estimation of the determinants for the BMI and determinants of being obese at all.
[![Pipeline Status](https://gitlab.com/ecouminho/DA/obesity-factors/badges/master/pipeline.svg)][gitlabci]
[![coverage report](https://gitlab.com/ecouminho/DA/obesity-factors/badges/master/coverage.svg)][coverage]